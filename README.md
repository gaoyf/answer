# README #

面试题答案

## 目录结构 ##
```
answer 根目录
├─sqls                       sql文目录
│ ├─001_create_table.sql     数据表创建sql
│ ├─002_insert_master.sql    初始化数据sql
|
├─Tools                      工具目录
│ ├─Db.class.php             pdo操作类
│
├─Answer.php                 面试题1~3
├─BusinessDateApi.php        面试题4: 获取营业时间接口
├─BusinessStatusApi.php      面试题4: 获取营业状态接口

```
## 接口文档 ##

### 获取营业状态接口 ###

#### 版本号    
 - v1.0
 
#### 简要描述  
- 根据商户id获取当前商户营业状态

#### 请求URL  
- 测试地址: ` http://localhost/answer/BusinessStatusApi.php `
  
#### 请求方式 ####

- POST 

#### 请求参数 ####

|数据项|参数名|数据类型|是否必填|所属父级|备注|
|-------|-------|-------|------|------|-----|
|商户id| businessId | string| 是 |-|双方约定 |


#### 请求参数示例 

``` 
{
  "businessId":"001"
}
``` 
 
#### 返回参数 
 
|数据项|数据类型|所属父级|说明|
|:-----  |:-----|-----|-----|
|returnCode |String |-|响应代码(SUCCESS/FAIL)  |
|errCode |String |-|错误代码 |
|errMsg |String |-|错误消息 |
|status |String |-|营业状态 0:未营业 1:营业中 |


#### 返回参数示例(正常) 
```
{
    "returnCode": "SUCCESS",
    "status": "0"
}
```
 
#### 返回参数示例(错误) 
```
{
    "returnCode": "FAIL",
    "errorCode": "500",
    "errMsg": "禁止访问"
}
```
#### 错误代码 

|错误代码|错误描述|
|-------|-------|
|101|商户不存在|
|500|禁止访问|

### 获取营业时间接口 ###
#### 版本号    
 - v1.0
 
#### 简要描述  
- 根据商户id获取本周营业时间详细

#### 请求URL 
- 测试地址: ` http://localhost/answer/BusinessDateApi.php `
  
#### 请求方式 

- POST 

#### 请求参数 

|数据项|参数名|数据类型|是否必填|所属父级|备注|
|-------|-------|-------|------|------|-----|
|商户id| businessId | string| &radic;|-|双方约定 |


#### 请求参数示例 

``` 
{
  "businessId":"001"
}
``` 
 
#### 返回参数 
 
|数据项|数据类型|所属父级|说明|
|:-----  |:-----|-----|-----|
|returnCode |String |-|响应代码(SUCCESS/FAIL)  |
|errCode |String |-|错误代码 |
|errMsg |String |-|错误消息 |
|desc |Array |-|营业时间明细 |


#### 返回参数示例(正常) 
```
{
    "returnCode": "SUCCESS",
    "desc": [
        "周一 06:00:00-22:00:00 营业",
        "周二 08:00:00-16:00:00 营业",
        "周三休息",
        "周四 08:00:00-15:00:00 营业",
        "周五 08:00:00-15:00:00,17:00:00-22:00:00 营业",
        "周六 08:00:00-22:00:00 营业",
        "周日 08:00:00-22:00:00 营业"
    ]
}
```
 
#### 返回参数示例(错误) 
```
{
    "returnCode": "FAIL",
    "errorCode": "500",
    "errMsg": "禁止访问"
}
```
#### 错误代码 

|错误代码|错误描述|
|-------|-------|
|101|商户不存在|
|500|禁止访问|


## 面试题4说明 ##
```
这道题我建了四张表，其中商户表仅仅是为了验证商户正确性而建立.
其余三张表都是营业时间表,要实现这样的接口功能，还需要两个定时任务进行协同
```
名称 | 启动策略 | 执行逻辑
---|--- | ---
定时任务一 | 每天零点执行   | 将总表中当天数据更新到日时间表中
定时任务二 | 每周一零点执行 | 将总表中当前一周数据更新到周时间表中
