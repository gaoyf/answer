<?php

/**
 * 面试题: 1-3
 *
 * @author GaoYongFu
 */

header("Content-type: text/html; charset=utf-8");
include './Tools/Db.class.php';

// 时区设置
date_default_timezone_set('Asia/Shanghai');

// 面试题1
answerOne();

// 面试题2

// 首次打开网址
// 1. dns解析: 域名->ip [优先使用本地host]
// 2. 浏览器(客户端)发送tcp连接请求到ip所在服务器(服务端)
// 3. ip所在服务器(服务端)接收到请求后通知浏览器(客户端)可以进行连接
// 4. 浏览器(客户端)接收到可以连接后,再次请求ip所在服务器(服务端)
// 5. ip所在服务器(服务端)根据内部业务逻辑组合数据返回给浏览器(客户端)
// 6. 浏览器(客户端)接收到数据,页面出现了

// 再次打开网址区别
// 如果存在缓存数据(数据库缓存,redis缓存或者文件缓存以及其他缓存),则优先读取,提升速度,三次握手还是正常进行,上述
// 第5步处理时间将小于第一次访问的时间

echo "<hr />";
// 面试题3
answerThree();



/**
 * 1 到 100 循环
 * 3 的倍数输出 Ping
 * 5 的倍数输出 Pong
 * 如果是 3 和 5 的倍数输出 PingPong。
 * 否则输出数字
 */
function answerOne() {
    for ($i = 1; $i <= 100; $i++) {
        switch ($i) {
            case $i % 3 == 0 && $i % 5 == 0:
                echo "PingPong<br />";
                break;
            case $i % 3 == 0:
                echo "Ping<br />";
                break;
            case $i % 5 == 0:
                echo "Pong<br />";
                break;
            default:
                echo $i . "<br />";
        }
    }
}

/**
 * 用户签到发勋章的实现
 */
function answerThree()
{
    // 默认登录
    $is_login = true;
    // 默认登录用户uid
    $uid = 1;
    if ($is_login) {
        // 判断用户是否已签到
        if (hasSign($uid)) {
            echo '用户今日已签到!';
            return;
        }
        // 用户是否有签到记录
        $data = getSign($uid);
        if (!empty($data)) {
            // 用户前一天是否签到
            if (strcmp($data['last_sign_date'], date("Y-m-d", strtotime("-1 day"))) == 0) {
                // 连续签到次数+1
                $data['continue_cnt'] += 1;
                // 是否连续60天签到
                if ($data['continue_cnt'] % 60 == 0) {
                    // 勋章个数+1
                    $data['medal_total'] += 1;
                }
            } else {
                // 连续签到次数初始化
                $data['continue_cnt'] = 1;
            }

            // 最新签到时间
            $data['last_sign_date'] = date('Y-m-d', time());

            // 更新人(默认为1)
            $data['update_by'] = 1;

            updateSign($data, $uid);

            echo '签到成功';
        } else {
            // 组装初始化数据
            $data['uid'] = $uid;
            $data['medal_total'] = 0;
            $data['continue_cnt'] = 1;
            $data['last_sign_date'] = date('Y-m-d', time());
            $data['del_flag'] = 0;
            $data['create_at'] = date('Y-m-d H:i:s', time());
            $data['create_by'] = 1;
            $data['update_by'] = 1;

            insertSign($data);
        }
    }
}

/**
 * 获取签到记录
 *
 * @param $uid   uid
 * @return array 签到记录
 */
function getSign($uid) {
    $db = new Db();
    $sql = 'SELECT 
                id,
                uid,
                medal_total,
                continue_cnt,
                last_sign_date 
            FROM 
                answer_sign 
            WHERE 
                UID = '.$uid.' 
            AND 
                DEL_FLAG = 0';
    $data = $db->query($sql);
    return $data;
}

/**
 * 更新签到记录
 *
 * @param $data 更新数据
 * @param $uid  uid
 */
function updateSign($data, $uid) {
    $db = new Db();
    // 更新数据
    $sql = 'UPDATE 
                answer_sign 
            SET 
                medal_total = ' . $data['medal_total'] . ', 
                continue_cnt = ' . $data['continue_cnt'] . ',
                last_sign_date = "' . $data['last_sign_date'] . '" 
            WHERE 
                uid = ' . $uid;

    $db->exec($sql);
}

/**
 * 插入签到记录
 *
 * @param $data 签到记录
 */
function insertSign($data) {
    $db = new Db();
    // 初始化签到数据
    $sql = 'INSERT INTO 
                answer_sign 
                (
                    uid, 
                    medal_total, 
                    continue_cnt, 
                    last_sign_date, 
                    del_flag, 
                    create_at, 
                    create_by, 
                    update_by
                ) 
            VALUES 
                (
                    ' . $data['uid'] . ',
                    ' . $data['medal_total'] . ',
                    ' . $data['continue_cnt'] . ',
                    "' . $data['last_sign_date'] . '",
                    ' . $data['del_flag'] . ',
                    "' . $data['create_at'] . '",
                    ' . $data['create_by'] . ',
                    ' . $data['update_by'] .
                ')';

    $db->exec($sql);
}

/**
 * 判断用户今日是否已签到
 *
 * @param $uid  uid
 * @return bool true/false
 */
function hasSign($uid) {
    $db = new Db();
    $date = date('Y-m-d', time());

    $sql = 'SELECT 
                last_sign_date 
            FROM 
                answer_sign 
            WHERE 
                UID = '.$uid.'  
            AND 
                DEL_FLAG = 0';
    $data = $db->query($sql);

    if (strcmp($date, $data['last_sign_date']) == 0) {
        return true;
    }
    return false;

}