<?php
/**
 * 工具: 数据库操作
 *
 * @author GaoYongFu
 */

Class Db{
    // pdo对象
    public static $pdo = null;
    // 主机地址
    public static $host = "127.0.0.1";
    // 数据库名称
    public static $dbName = "answer";
    // 用户名
    public static $dbUser = "root";
    // 密码
    public static $dbPwd = "root";

    public function __construct(){
        if(is_null(self::$pdo)){
            try {
                //实例化pdo对象，赋值给self::$pdo
                self::$pdo=new Pdo('mysql:host='.self::$host.';dbname='.self::$dbName,self::$dbUser,self::$dbPwd);
                //设置编码格式
                self::$pdo->query('set names utf8');
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            } catch (PdoException $e) {
                die($e->getMessage());
            }
        }
    }

    /** 查询
     * @param $sql   sql文
     * @return array 结果
     */
    public function query($sql){
        try {
            //执行查询语句
            $res=self::$pdo->query($sql);
            //获取结果
            $rows=$res->fetchAll(PDO::FETCH_ASSOC);
            // 结果集处理
            if (sizeof($rows) == 1) {
                return $rows[0];
            }
            //返回结果
            return $rows;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /** 增、删、改
     * @param $sql sql文
     * @return int 受影响行数
     */
    public function exec($sql){
        try {
            $rows=self::$pdo->exec($sql);
            return $rows;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}