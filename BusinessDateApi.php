<?php
/**
 * 面试题4: 获取营业时间接口
 *
 * @author GaoYongFu
 */

include './Tools/Db.class.php';

echo api($GLOBALS['HTTP_RAW_POST_DATA']);


/** 业务逻辑
 *
 * @param $request 请求参数
 * @return string  返回值
 */
function api($request) {
    // json->array
    $request = json_decode($request, true);

    if (isset($request['businessId'])) {
        // 校验商户是否存在
        if (!validateBusiness($request['businessId'])) {
            $response['returnCode'] = 'FAIL';
            $response['errorCode'] = '101';
            $response['errMsg'] = '商户不存在';
            return json_encode($response);
        }

        $response['returnCode'] = 'SUCCESS';

        // 本周开始日期与结束日期
        $week = getCurrentWeekDate();

        // 商户营业时间明细
        $response['desc'] = getBusinessDate($request['businessId'], $week);

    } else {
        $response['returnCode'] = 'FAIL';
        $response['errorCode'] = '500';
        $response['errMsg'] = '禁止访问';
    }
    return json_encode($response);
}

/**
 * 商户校验
 * @param $businessId 商户ID
 * @return bool       true/false
 */
function validateBusiness($businessId) {
    $db = new Db();
    $sql = 'SELECT 
                id 
            FROM 
                answer_business 
            WHERE 
                business_id = "'.$businessId.'" 
            AND 
                DEL_FLAG = 0';
    $data = $db->query($sql);
    if (empty($data)) {
        return false;
    }
    return true;
}

/**
 * 获取本周开始日期与结束日期
 *
 * @return mixed  日期数组
 */
function getCurrentWeekDate() {
    // 当天时间
    $currentDate = date("Y-m-d");

    // 当天是本周第几天
    $w = date('w',strtotime($currentDate));

    $week['start'] = date('Y-m-d',strtotime("$currentDate -".($w ? $w - 1 : 6).' days'));

    $week['end'] = date('Y-m-d',strtotime($week['start'].' +6 days'));

    return $week;

}

/**
 * 获取商户本周营业时间明细
 *
 * @param $businessId  商户ID
 * @param $week        本周开始时间结束时间
 * @return array       营业时间明细
 */
function getBusinessDate($businessId, $week) {
    $db = new Db();
    $sql = 'SELECT 
                temp.business_id,
                temp.`date`,
                temp.begin_time,
                temp.end_time 
            FROM 
                (
                    SELECT 
                        business_id,
                        `date`,
                        begin_time,
                        end_time
                    FROM
                        answer_business_time_week
                    WHERE 
                        business_id = "'.$businessId.'"
                    AND
                        `date` >= "'.$week['start'].'"
                    AND
                        `date` <= "'.$week['end'].'"
                    AND 
                        DEL_FLAG = 0
                ) AS temp 
            ORDER BY
                temp.`date`, temp.begin_time';

    $data = $db->query($sql);

    $businessDate = ['周一','周二','周三','周四','周五','周六','周日'];

    // 是否存在本周营业时间
    if (!empty($data)) {
        // 获取每天营业时间
        foreach ($data as $v) {
            // 当前时间为本周第几天
            $currentWeek = date('w', strtotime($v['date']));
            // 保证日期索引一致
            if ($currentWeek == 0) {
                $currentWeek = 6;
            } else {
                $currentWeek -= 1;
            }
            // 每天营业时间
            $businessTime[$currentWeek][] = $v['begin_time'] . '-' . $v['end_time'];
        }

        // 组合数据
        foreach ($businessDate as $k => $v) {
            if (empty($businessTime[$k])) {
                $businessDate[$k] = $v . '休息';
            } else{
                $businessDate[$k] = $v . ' '.implode(',', $businessTime[$k]) . ' 营业';
            }
        }

    } else {
        foreach ($businessDate as $k=>$v) {
            $businessDate[$k]  = $v . '休息';
        }
    }

    return $businessDate;
}