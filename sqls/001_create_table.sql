# ------------------------------------------------------------
# 商户表
# ------------------------------------------------------------
DROP TABLE IF EXISTS `answer_business`;

CREATE TABLE `answer_business` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `business_id` varchar(20) NOT NULL COMMENT '商户id',
  `business_name` varchar(20) NOT NULL COMMENT '商户名称',
  `del_flag` tinyint(1) NOT NULL COMMENT '删除标志 0:未删除 1:已删除',
  `create_at` datetime NOT NULL COMMENT '创建时间',
  `create_by` int(11) NOT NULL COMMENT '创建人',
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` int(11) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8 COMMENT='商户表';


# ------------------------------------------------------------
# 营业时间-总表
# ------------------------------------------------------------
DROP TABLE IF EXISTS `answer_business_time`;

CREATE TABLE `answer_business_time` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `business_id` varchar(20) NOT NULL COMMENT '商户id',
  `date` date DEFAULT NULL COMMENT '当天时间',
  `begin_time` time DEFAULT NULL COMMENT '营业开始时间',
  `end_time` time DEFAULT NULL COMMENT '营业结束时间',
  `del_flag` tinyint(1) NOT NULL COMMENT '删除标志 0:未删除 1:已删除',
  `create_at` datetime NOT NULL COMMENT '创建时间',
  `create_by` int(11) NOT NULL COMMENT '创建人',
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` int(11) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  KEY `IDX_BUSINESS_TIME` (`business_id`,`date`) USING BTREE
) ENGINE=InnoDB CHARSET=utf8 COMMENT='营业时间-总表';



# ------------------------------------------------------------
# 营业时间-日时间表
# ------------------------------------------------------------
DROP TABLE IF EXISTS `answer_business_time_day`;

CREATE TABLE `answer_business_time_day` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `business_id` varchar(20) NOT NULL COMMENT '商户id',
  `begin_time` time DEFAULT NULL COMMENT '营业开始时间',
  `end_time` time DEFAULT NULL COMMENT '营业结束时间',
  `del_flag` tinyint(1) NOT NULL COMMENT '删除标志 0:未删除 1:已删除',
  `create_at` datetime NOT NULL COMMENT '创建时间',
  `create_by` int(11) NOT NULL COMMENT '创建人',
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` int(11) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  KEY `IDX_BUSINESS_TIME_DAY` (`business_id`,`begin_time`,`end_time`) USING BTREE
) ENGINE=InnoDB CHARSET=utf8 COMMENT='营业时间-日时间表';



# ------------------------------------------------------------
# 营业时间-周时间表
# ------------------------------------------------------------
DROP TABLE IF EXISTS `answer_business_time_week`;

CREATE TABLE `answer_business_time_week` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `business_id` varchar(20) NOT NULL COMMENT '商户id',
  `date` date DEFAULT NULL COMMENT '当天时间',
  `begin_time` time DEFAULT NULL COMMENT '营业开始时间',
  `end_time` time DEFAULT NULL COMMENT '营业结束时间',
  `del_flag` tinyint(1) NOT NULL COMMENT '删除标志 0:未删除 1:已删除',
  `create_at` datetime NOT NULL COMMENT '创建时间',
  `create_by` int(11) NOT NULL COMMENT '创建人',
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` int(11) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  KEY `IDX_BUSINESS_TIME_WEEK` (`business_id`,`date`) USING BTREE
) ENGINE=InnoDB CHARSET=utf8 COMMENT='营业时间-周时间表';


# ------------------------------------------------------------
# 答案-签到表
# ------------------------------------------------------------
DROP TABLE IF EXISTS `answer_sign`;

CREATE TABLE `answer_sign` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增唯一ID',
  `uid` bigint(20) NOT NULL COMMENT '用户uid',
  `medal_total` int(11) DEFAULT NULL COMMENT '已获取勋章总数(此字段临时放置,应属于个人信息)',
  `continue_cnt` int(11) DEFAULT NULL COMMENT '连续签到次数',
  `last_sign_date` date DEFAULT NULL COMMENT '最后一次签到日期',
  `del_flag` tinyint(1) NOT NULL COMMENT '删除标志 0:未删除 1:已删除',
  `create_at` datetime NOT NULL COMMENT '创建时间',
  `create_by` int(11) NOT NULL COMMENT '创建人',
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` int(11) NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='答案-签到记录表';