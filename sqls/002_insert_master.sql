# ------------------------------------------------------------
# 初始数据: 商户表
# ------------------------------------------------------------
DELETE FROM answer_business;

INSERT INTO `answer_business` (`id`, `business_id`, `business_name`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('1', '001', '测试商户01', '0', '2017-09-02 10:18:38', '1', '2017-09-02 10:18:42', '1');


# ------------------------------------------------------------
# 初始数据: 营业时间-总表
# ------------------------------------------------------------
DELETE FROM answer_business_time;

INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('1', '001', '2017-09-01', '08:00:00', '15:00:00', '0', '2017-09-03 08:44:50', '1', '2017-09-03 08:44:57', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('2', '001', '2017-09-01', '17:00:00', '22:00:00', '0', '2017-09-03 08:45:41', '1', '2017-09-03 08:45:45', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('3', '001', '2017-09-02', '08:00:00', '22:00:00', '0', '2017-09-03 08:46:22', '1', '2017-09-03 08:46:32', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('4', '001', '2017-09-03', '08:00:00', '22:00:00', '0', '2017-09-03 08:47:01', '1', '2017-09-03 08:47:06', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('5', '001', '2017-08-31', '08:00:00', '15:00:00', '0', '2017-09-03 08:47:45', '1', '2017-09-03 08:47:49', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('6', '001', '2017-08-29', '08:00:00', '16:00:00', '0', '2017-09-03 08:48:21', '1', '2017-09-03 08:48:25', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('7', '001', '2017-08-28', '06:00:00', '22:00:00', '0', '2017-09-03 08:49:08', '1', '2017-09-03 08:49:14', '1');
INSERT INTO `answer_business_time` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('8', '001', '2017-09-04', '06:00:00', '22:00:00', '0', '2017-09-03 08:49:59', '1', '2017-09-03 08:50:03', '1');

# ------------------------------------------------------------
# 初始数据: 营业时间-日时间表
# ------------------------------------------------------------
DELETE FROM answer_business_time_day;

INSERT INTO `answer_business_time_day` (`id`, `business_id`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('1', '001', '08:00:00', '22:00:00', '0', '2017-09-03 08:51:22', '1', '2017-09-03 08:54:57', '1');

# ------------------------------------------------------------
# 初始数据: 营业时间-周时间表
# ------------------------------------------------------------
DELETE FROM answer_business_time_week;

INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('1', '001', '2017-09-01', '08:00:00', '15:00:00', '0', '2017-09-03 08:44:50', '1', '2017-09-03 08:44:57', '1');
INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('2', '001', '2017-09-01', '17:00:00', '22:00:00', '0', '2017-09-03 08:45:41', '1', '2017-09-03 08:45:45', '1');
INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('3', '001', '2017-09-02', '08:00:00', '22:00:00', '0', '2017-09-03 08:46:22', '1', '2017-09-03 08:46:32', '1');
INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('4', '001', '2017-09-03', '08:00:00', '22:00:00', '0', '2017-09-03 08:47:01', '1', '2017-09-03 08:47:06', '1');
INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('5', '001', '2017-08-31', '08:00:00', '15:00:00', '0', '2017-09-03 08:47:45', '1', '2017-09-03 08:47:49', '1');
INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('6', '001', '2017-08-29', '08:00:00', '16:00:00', '0', '2017-09-03 08:48:21', '1', '2017-09-03 08:48:25', '1');
INSERT INTO `answer_business_time_week` (`id`, `business_id`, `date`, `begin_time`, `end_time`, `del_flag`, `create_at`, `create_by`, `update_at`, `update_by`) VALUES ('7', '001', '2017-08-28', '06:00:00', '22:00:00', '0', '2017-09-03 08:49:08', '1', '2017-09-03 08:49:14', '1');
