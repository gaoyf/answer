<?php
/**
 * 面试题4: 获取营业状态接口
 *
 * @author GaoYongFu
 */

include './Tools/Db.class.php';

echo api($GLOBALS['HTTP_RAW_POST_DATA']);


/** 业务逻辑
 *
 * @param $request 请求参数
 * @return string  返回值
 */
function api($request) {
    // json->array
    $request = json_decode($request, true);

    if (isset($request['businessId'])) {
        // 校验商户是否存在
        if (!validateBusiness($request['businessId'])) {
            $response['returnCode'] = 'FAIL';
            $response['errorCode'] = '101';
            $response['errMsg'] = '商户不存在';
            return json_encode($response);
        }

        $response['returnCode'] = 'SUCCESS';

        // 是否处于营业状态
        if (isActive($request['businessId'])) {
            $response['status'] = "1";
        } else {
            $response['status'] = "0";
        }

    } else {
        $response['returnCode'] = 'FAIL';
        $response['errorCode'] = '500';
        $response['errMsg'] = '禁止访问';
    }
    return json_encode($response);
}

/**
 * 商户校验
 * @param $businessId 商户ID
 * @return bool       true/false
 */
function validateBusiness($businessId) {
    $db = new Db();
    $sql = 'SELECT 
                id 
            FROM 
                answer_business 
            WHERE 
                business_id = "'.$businessId.'" 
            AND 
                DEL_FLAG = 0';
    $data = $db->query($sql);
    if (empty($data)) {
        return false;
    }
    return true;
}

/**
 * 判断商户是否处于营业状态
 *
 * @param $businessId 商户ID
 * @return bool       true/false
 */
function isActive($businessId) {
    $db = new Db();

    $sql = 'SELECT 
                id 
            FROM 
                answer_business_time_day 
            WHERE 
                business_id = "'.$businessId.'"
            AND
                begin_time <= curtime()
            AND
                end_time >= curtime()
            AND 
              DEL_FLAG = 0';

    $data = $db->query($sql);

    if (empty($data)) {
        return false;
    }

    return true;
}